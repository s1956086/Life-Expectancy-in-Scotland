https://sway.office.com/RQcyyFAut7ySRiNF?ref=Link

## **Git基础命令**
$ ssh-keygen -t ed25519 -C "...@...."	创建SSH Key
$ cat ~/.ssh/id_ed25519.pub | clip	添加SSH Key至账号（在剪贴板里，打开网页并粘贴）
$ git init			在当前文件夹下新建一个本地git代码库
$ git add .			将所有修改（新增/修改/删除）上传至git仓库
$ git commit -m "..."		只能在add完成之后执行，将上次提交至今的所有修改提交为一次提交，计入git历史记录
$ git log			查看提交历史记录，按Q退出
$ git push			第一次操作过之后，后面只需要执行这一句，就可以将当前分支直接推到远程
$ git pull			从远程拉取最新代码
$ git reset --hard 版本号	回退到某版本（版本号写前几个字即可）
$ git clone git@...		将项目克隆至本地
$ ls			显示当前文件夹下所有文件
$ cd newDir/		进入文件夹
$ cd			返回根目录
$ mkdir folderName		创建新文件夹
$ git config --global core.autocrlf true	解决换行符一致性问题

## **Atom操作**
·打开项目文件
·进行修改并保存后，点击右下角Git
·双击或点击Stage All确认更改
·输入Commit Message并进行Commit
·点击Push，上传至GitLab
·点击Fetch，与云端进行比较
·点击Pull，更新文件

git@git.ecdf.ed.ac.uk:s1956086/Life-Expectancy-in-Scotland.git